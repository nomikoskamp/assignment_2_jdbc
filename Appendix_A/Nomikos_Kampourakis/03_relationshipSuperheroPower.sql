--creates the linked table superhero_power to link superhero table with power table 
CREATE TABLE superhero_power(
	hero_id int REFERENCES superhero,
	pow_id int REFERENCES power,
	PRIMARY KEY (hero_id, pow_id)
);


