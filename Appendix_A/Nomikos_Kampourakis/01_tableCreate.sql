--Creation of tables superhero, assistan, power

DROP TABLE IF EXISTS superhero;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS power;


CREATE TABLE superhero (
    hero_id serial PRIMARY KEY,
    hero_name varchar(15) NOT NULL,
    hero_alias varchar(15) NOT NULL,
	hero_origin varchar(15) NOT NULL
);

CREATE TABLE assistant (
    ass_id serial PRIMARY KEY,
    ass_name varchar(15) NOT NULL
);

CREATE TABLE power (
    pow_id serial PRIMARY KEY,
    pow_name varchar(15) NOT NULL,
	pow_descr varchar(100) NOT NULL
);