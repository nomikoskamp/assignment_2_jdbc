--insert 4 powers to the power table

INSERT INTO power  (pow_name, pow_descr) VALUES ('Agility', 'Ability to move quickly and easily');
INSERT INTO power  (pow_name, pow_descr) VALUES ('Strength', 'The quality or state of being physically strong');
INSERT INTO power  (pow_name, pow_descr) VALUES ('Speed', 'The state of moving quickly or the capacity for rapid motion');
INSERT INTO power  (pow_name, pow_descr) VALUES ('Regeneration', 'Regeneration is the process of renewal');


--assigns power to a superhero 

INSERT INTO superhero_power (hero_id , pow_id) VALUES (1,1);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (1,2);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (2,2);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (2,3);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (2,4);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (3,2);
INSERT INTO superhero_power (hero_id , pow_id) VALUES (3,3);

