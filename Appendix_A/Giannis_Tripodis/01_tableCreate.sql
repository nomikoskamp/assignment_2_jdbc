DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS superhero;
DROP TABLE IF EXISTS superpower;

CREATE TABLE superhero(
	sup_id serial PRIMARY KEY,
	sup_name varchar(50) NOT NULL,
	sup_alias varchar(50) NOT NULL,
	sup_origin varchar(50)
);
CREATE TABLE assistant(
	Ass_id serial PRIMARY KEY,
	Ass_name varchar(50) NOT NULL
);
CREATE TABLE superpower(
	pow_id serial PRIMARY KEY,
	pow_name varchar(50) NOT NULL,
	pow_description varchar(150)
);