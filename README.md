# Assignment 2: Create a database and access it

## Table of Contents

- [Executive summary](#executive-summary)
- [Technologies](#technologies)
- [General Information](#general-information)
- [Appendix A](#appendix-a)
- [Appendix B](#appendix-b)
- [Maintainers](#maintainers)
- [License](#license)

## Executive summary

This is a project which is divided into two main parts:
- [ ] **Appendix A:** SQL scripts to create a database.  
- [ ] **Appendix B:** Reading and manipulating data in Spring with JDBC.

## Technologies

- [ ] Intellij Ultimate
- [ ] Postgres and PgAdmin
- [ ] Java 17
- [ ] Postgres SQL driver dependency


## General Information

### Appendix A
In this part several scripts were made which can be run to create a database name SuperheroDb,
setup some tables in the database, add relationships to the tables, and then populate the tables with data.

#### Entity Diagram

The following **entity diagram** captures the association of the tables:

![alt text1](pics/Entity_Diagram.png "Title Text")

### Appendix B
This part of the project deals with manipulating SQL data of Chinook database in Spring using the JDBC with 
the PostgreSQL driver. Chinook models the iTunes database of customers purchasing songs.
A Spring Boot application is created including a repository pattern to interact with the database.

#### Functionality

###### CrudRepository
- [ ] `getAll` Reads all the customers in the database.
- [ ] `getById` Reads a specific customer from the database by Id.
- [ ] `create` Add a new customer to the database.
- [ ] `update` Update an existing customer.

###### CustomerRepository
- [ ] `getByName` Reads a specific customer by name using LIKE keyword.
- [ ] `getSubSet` Returns a page of customers from the database.

###### ReportRepository
- [ ] `getCountryWithMostCustomers` Returns the country with the most customers.
- [ ] `getHigherSpender` Returns the customer who is the highest spender.
- [ ] `getPopularGenre` For a given customer,returns their most popular genre


#### Models                           

- [ ] Customer
- [ ] CustomerCountry
- [ ] CustomerGenre
- [ ] CustomerSpender

## Maintainers

[Nomikos Kampourakis ](https://gitlab.com/nomikoskamp)

[Giannis Tripodis](https://gitlab.com/giatrip)

## License
Giannis Tripodis

Nomikos Kampourakis

@ 2023