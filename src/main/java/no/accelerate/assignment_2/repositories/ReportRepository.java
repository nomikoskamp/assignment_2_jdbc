package no.accelerate.assignment_2.repositories;

import no.accelerate.assignment_2.models.CustomerCountry;
import no.accelerate.assignment_2.models.CustomerGenre;
import no.accelerate.assignment_2.models.CustomerSpender;

import java.util.List;

public interface ReportRepository  {
    List<CustomerCountry> getCountryWithMostCustomers();
    List<CustomerSpender> getHigherSpender();
    List<CustomerGenre> getPopularGenre(int id);

}
