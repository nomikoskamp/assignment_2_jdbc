package no.accelerate.assignment_2.repositories;

import no.accelerate.assignment_2.models.Customer;
import no.accelerate.assignment_2.models.CustomerCountry;
import no.accelerate.assignment_2.models.CustomerGenre;
import no.accelerate.assignment_2.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository, ReportRepository {

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    public void testConnection() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("Connected");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Customer> getAll() {
        // Reads all the customers in the database
        String sql = "SELECT * FROM customer";
        List<Customer> customerList = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while (result.next()) {
                // Creates a new customer with Id, first name, last name, country,
                // postal code, phone number and email
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                // Adding each customer object into a list
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public Customer getById(Integer id) {
        Customer customer = null;
        // Reads a specific customer from the database (by Id)
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                // Creates a new customer with Id, first name, last name, country,
                // postal code, phone number and email
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public List<Customer> getByName(String name) {
        List<Customer> customerList = new ArrayList<>();
        // Reads a specific customer by name using LIKE keyword to help for partial matches
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                "FROM customer " +
                "WHERE first_name  LIKE ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Passing argument to the query (%name%)
            statement.setString(1, '%' + name + '%');
            // Execute statement
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                // Creates a new customer with Id, first name, last name, country,
                // postal code, phone number and email
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                // Add customer objects into a list, query will return
                // more than one customer in some cases
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public List<Customer> getSubSet(int limit, int offset) {
        // Returns a page of customers from the database
        // Takes in limit and offset as parameters to get a subset of the customer data
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                "FROM customer LIMIT  ?  OFFSET ?";
        List<Customer> customerList = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while (result.next()) {
                // Creates a new customer with Id, first name, last name, country,
                // postal code, phone number and email
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email"));
                // Add customer objects into a list
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }

    @Override
    public void create(Customer customer) {
        // Add a new customer to the database
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) " +
                "VALUES   (((SELECT MAX( customer_id ) FROM customer)+1), ?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postal_code());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Customer customer) {
        // Updates an existing customer (by Id)
        String sql = "UPDATE customer " +
                "SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? " +
                "WHERE customer_id = ? ";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postal_code());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.customer_id());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<CustomerCountry> getCountryWithMostCustomers() {
        // Returns the country with the most customers
        String sql = "SELECT country, COUNT(*) AS total " +         // Selects countries from customer's table
                "FROM customer  " +                                 // calculates the total appearances of each country
                "GROUP BY country " +                               // at the second column, then orders table by country name
                "ORDER BY total DESC " +                            // and returns country or countries in case of a tie
                "FETCH FIRST ROW WITH TIES ";                       // with max appearances
        ArrayList<CustomerCountry> countryList = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while (result.next()) {
                // Creates a new customerCountry object country name
                // and total appearances of the country
                CustomerCountry country = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("total")
                );
                // Add one ore more countries in case of tie into a list
                countryList.add(country);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countryList;
    }

    @Override
    public List<CustomerSpender> getHigherSpender() {
        Integer id = -1;
        List <CustomerSpender> customerList = new ArrayList<>();
        // Returns Customer who is the highest spender
        String sql = "SELECT customer.customer_id, SUM(invoice.total) as total " + // Selects customer's id and total invoices for every customer
                "FROM customer, invoice " +                                        // from tables of customers and invoice
                "WHERE customer.customer_id = invoice.customer_id " +              // using foreign and primary keys regarding
                "GROUP BY customer.customer_id " +                                 // the association of two tables
                "ORDER BY total DESC " +                                           // orders table by total invoice
                "FETCH FIRST ROW WITH TIES ";                                      // return customer id and rows with max invoice
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                // id stores the customer_id who is the highest spender
                id = result.getInt("customer_id");
                // Creates a customerSpender object
                CustomerSpender customer = new CustomerSpender(
                        this.getById(id),
                        result.getFloat("total")) ;
                // Add customerSpender object or objects in case of tie in to a list
                customerList.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerList;
    }


    public List<CustomerGenre> getPopularGenre(int id) {
        // Returns the most popular genre
        String sql = "SELECT genre.name , COUNT(*) AS amount " +                            // Selects genre name and calculates
                "FROM (((genre " +                                                          // the amount of it from tables genre, track
                "INNER JOIN track ON genre.genre_id = track.genre_id) " +                   // invoice_line, invoice, customer using foreign
                "INNER JOIN invoice_line ON track.track_id = invoice_line.track_id) " +     // and primary keys regarding their association
                "INNER JOIN invoice ON invoice.invoice_id = invoice_line.invoice_id) " +    // orders the table by the genre name
                "INNER JOIN customer ON invoice.customer_id = ? " +                         // and returns the genre and the max amount of genre
                "GROUP BY genre.name " +
                "ORDER BY amount DESC " +
                "FETCH FIRST ROW WITH TIES ";
        List<CustomerGenre> genreList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                // Creates a customerGenre object
                CustomerGenre genre =new CustomerGenre(
                        result.getString("name"),
                        result.getInt("amount")
                );
                // Add customerGenre object or objects in case of tie into a list
                genreList.add(genre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genreList;
    }

}