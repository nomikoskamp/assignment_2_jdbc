package no.accelerate.assignment_2.repositories;


import no.accelerate.assignment_2.models.Customer;

import java.util.List;

public interface CustomerRepository extends CrudRepository<Integer, Customer> {

    List<Customer> getByName(String name);
    List<Customer> getSubSet(int limit, int offset);

}
