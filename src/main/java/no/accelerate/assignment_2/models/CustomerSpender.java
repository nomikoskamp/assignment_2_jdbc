package no.accelerate.assignment_2.models;

public record CustomerSpender(
        Customer customer,
        float amount) {
}
