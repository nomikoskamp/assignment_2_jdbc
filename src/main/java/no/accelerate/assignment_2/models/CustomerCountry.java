package no.accelerate.assignment_2.models;

public record CustomerCountry(String country, int total) {
}
