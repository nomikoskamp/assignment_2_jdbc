package no.accelerate.assignment_2.runner;

import no.accelerate.assignment_2.models.Customer;
import no.accelerate.assignment_2.repositories.CustomerRepositoryImpl;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChinookRunner implements ApplicationRunner {
    private final CustomerRepositoryImpl customerRep;

    public ChinookRunner(CustomerRepositoryImpl customerRep) {
        this.customerRep = customerRep;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        customerRep.testConnection();

//            Uncomment below to check results

////                  PRINT ALL CUSTOMERS
//        for (int i = 0; i < customerRep.getAll().size(); i++) {
//            System.out.println(customerRep.getAll().get(i));
//        }
//
////                  PRINT BY ID
//        System.out.println(customerRep.getById(3));
//
//
////                  PRINT BY NAME
//        List<Customer> customerList = customerRep.getByName("len");
//        for (int i=0;i<customerList.size();i++)
//            System.out.println(customerList.get(i));
//
//
////                  PRINT A SUBSET OF CUSTOMERS
//        List<Customer> customerList2 = customerRep.getSubSet(5,10);
//        for (int i=0;i<customerList.size();i++){
//            System.out.println(customerList2.get(i));
//        }
//
//
////                  CREATE A NEW CUSTOMER
//        Customer customer = new Customer(1,"Julian","Alvarez","Argentina","15235","6965434211","julianAlva@yahoo.com");
//        customerRep.create(customer);
//        System.out.println(customerRep.getByName("Julian"));
//
//
////                  CUSTOMER UPDATE
//        Customer customer2 = new Customer(60,"Julio","Cesar","Brazil","17835","6921212121","juliocesar@yahoo.com");
//        customerRep.update(customer2);
//        System.out.println(customerRep.getById(60));
//
//
////                  PRINT THE COUNTRY WITH MOST CUSTOMERS
//        System.out.println(customerRep.getCountryWithMostCustomers());
//
//
////                  PRINT THE CUSTOMER WHO SPENDS MORE
//        System.out.println(customerRep.getHigherSpender());
//
//
////                  PRINT THE MOST POPULAR GENRE
//        System.out.println(customerRep.getPopularGenre(12));
  }
}